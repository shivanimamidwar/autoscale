provider "aws" {
  region = var.aws_region
}

variable "aws_region" {
  type = string
  default = "us-west-2"
}

variable "ami_id" {
  type = string
  default = "ami-05bfbece1ed5beb54"
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "asg_min_size" {
  type = number
  default = 1
}

variable "asg_max_size" {
  type = number
  default = 5
}

variable "asg_desired_capacity" {
  type = number
  default = 2
}

variable "subnet_ids" {
  type = list(string)
  default = ["subnet-0ec696f456694bfe1", "subnet-0d7d70dc2887883c6"]
}

resource "launch_template" "example" {
  image_id      = var.ami_id
  instance_type = var.instance_type

  # Additional configuration for the launch template goes here
}

resource "aws_autoscaling_group" "example" {
  name                 = "example-asg"
  launch_template      = aws_launch_template.id
  min_size             = var.asg_min_size
  max_size             = var.asg_max_size
  desired_capacity     = var.asg_desired_capacity
  vpc_zone_identifier = var.subnet_ids
}
